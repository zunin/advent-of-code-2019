function fuelForMass(mass: number): number {
    return Math.round((mass/3) - 0.5) - 2;
}

export async function part1(rawInput: string) {
    let moduleMasses = rawInput
        .split("\r\n")
        .map(x => Number.parseInt(x));

    let fuelRequired = moduleMasses.map(fuelForMass).reduce((sum, current) => sum += current)
    return fuelRequired;
}

export async function part2(rawInput: string) {
    let moduleMasses = rawInput
        .split("\r\n")
        .map(x => Number.parseInt(x));

    let fuelRequiredForMass = (await Promise.all(moduleMasses
        .map(mass => resursiveFueldForMass(mass))))
        .reduce((sum, current) => sum += current);
        
    return fuelRequiredForMass;
}

async function resursiveFueldForMass(mass: number): Promise<number> {
    let fuelMass = fuelForMass(mass);
    if (fuelMass <= 0) {
        return 0;
    }
    return await resursiveFueldForMass(fuelMass) + fuelMass;
}

