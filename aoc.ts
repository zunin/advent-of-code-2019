import * as day1 from './days/day1.ts'

import { readFileStr } from 'https://deno.land/std/fs/mod.ts';

async function getInput(inputName: string): Promise<string> {
    let file: string = await readFileStr(`./input/${inputName}.txt`);
    if (!file) {
        throw new Error("Input not found");
    }
    return file;
}

async function main() {
    console.log("--- Day 1: The Tyranny of the Rocket Equation ---");
    let moduleMasses = await getInput("day1");
    let day1pt1 = await day1.part1(moduleMasses);
    console.log(`Total fuel required for launch is ${day1pt1}`);
    let day2pt2 = await day1.part2(moduleMasses);
    console.log(`Total fuel required for launch (including fuel weight) is ${day2pt2}`);

}

main();
