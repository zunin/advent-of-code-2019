import { runTests, test } from "https://deno.land/std/testing/mod.ts";
import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import * as day1 from './days/day1.ts';


test({
    name: "Test day 1 part 1",
    async fn(): Promise<void> {
        let test1 = await day1.part1("12");
        assertEquals(test1, 2);

        let test2 = await day1.part1("14");
        assertEquals(test2, 2);

        let test3 = await day1.part1("1969");
        assertEquals(test3, 654);

        let test4 = await day1.part1("100756");
        assertEquals(test4, 33583);
    }
});

test({
    name: "Test day 1 part 2",
    async fn(): Promise<void> {
        let test1 = await day1.part2("14");
        assertEquals(test1, 2);

        let test2 = await day1.part2("1969");
        assertEquals(test2, 966);

        let test3 = await day1.part2("100756");
        assertEquals(test3, 50346);
    }
});


runTests();
